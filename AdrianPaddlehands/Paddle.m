//
//  Paddle.m
//  AdrianPaddlehands
//
//  Created by Ryan Salton on 27/08/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "Paddle.h"

@implementation Paddle

-(instancetype)initWithImageNamed:(NSString *)name
{
    self = [super initWithImageNamed:name];
    if(self){
        self.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(60, 10) center:CGPointMake(0, 0)];
        self.physicsBody.dynamic = NO;
//        self.physicsBody.restitution = 1.0;
//        self.physicsBody.friction = 0;
    }
    return self;
}

@end
