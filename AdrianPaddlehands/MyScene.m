//
//  MyScene.m
//  AdrianPaddlehands
//
//  Created by Ryan Salton on 25/08/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "MyScene.h"
#import "Player.h"
#import "Paddle.h"
#import "Enemy.h"

#define PADDLE_Y_MAX self.size.height * 0.5
#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)

static const uint32_t ballCategory = 0x1 << 1;
static const uint32_t paddleCategory = 0x1 << 2;
static const uint32_t wallCategory = 0x1 << 3;
//static const uint32_t particleCategory = 0x1 << 4;

@implementation MyScene {
    Paddle *leftPaddle;
    Paddle *rightPaddle;
    Player *player;
    
    SKShapeNode *leftArm;
    SKShapeNode *rightArm;
    
    NSInteger curveControlAdjust;
    NSInteger curveControlAdjustRight;
}

-(id)initWithSize:(CGSize)size {    
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        
        self.backgroundColor = [SKColor colorWithRed:0.9 green:0.55 blue:0.2 alpha:1.0];
        
        self.physicsWorld.gravity = CGVectorMake(0, 0);
        self.physicsWorld.contactDelegate = self;
        self.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:self.frame];
        self.physicsBody.restitution = 1.0;
        self.physicsBody.friction = 0;
        self.physicsBody.categoryBitMask = wallCategory;
        self.physicsBody.collisionBitMask = ballCategory;
        
        player = [Player spriteNodeWithImageNamed:@"player"];
        player.position = CGPointMake(size.width * 0.5, (player.size.height * 0.5) + 10);
        
        leftPaddle = [[Paddle alloc] initWithImageNamed:@"paddle-left"];
        leftPaddle.position = CGPointMake(size.width * 0.25, player.position.y + 60);
        leftPaddle.physicsBody.categoryBitMask = paddleCategory;
        leftPaddle.physicsBody.contactTestBitMask = ballCategory;
        leftPaddle.physicsBody.collisionBitMask = ballCategory;
        
        rightPaddle = [[Paddle alloc] initWithImageNamed:@"paddle-right"];
        rightPaddle.position = CGPointMake(size.width * 0.75, player.position.y + 60);
        rightPaddle.physicsBody.categoryBitMask = paddleCategory;
        rightPaddle.physicsBody.contactTestBitMask = ballCategory;
        rightPaddle.physicsBody.collisionBitMask = ballCategory;
        
        [self drawArms];
        
        [self addChild:player];
        [self addChild:leftPaddle];
        [self addChild:rightPaddle];
        
        [self createBalls];
        
        [self createEnemies];
        
    }
    return self;
}

-(void)createBalls
{
    CGFloat radius = 8.0;
    
    CGMutablePathRef circlePath = CGPathCreateMutable();
    CGPathAddArc(circlePath, NULL, 0, 0, radius, 0, M_PI * 2, YES);
    
    SKShapeNode *circleNode = [[SKShapeNode alloc] init];
    circleNode.path = circlePath;
    circleNode.position = CGPointMake(self.size.width * 0.5, self.size.height);
    circleNode.fillColor = [UIColor whiteColor];
    [self addChild:circleNode];
    
    circleNode.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:radius];
    circleNode.physicsBody.restitution = 1;
    circleNode.physicsBody.linearDamping = 0;
    circleNode.physicsBody.friction = 0;
    circleNode.physicsBody.categoryBitMask = ballCategory;
    circleNode.physicsBody.contactTestBitMask = paddleCategory;
    circleNode.physicsBody.collisionBitMask = paddleCategory | wallCategory;
    
    [circleNode.physicsBody applyImpulse:CGVectorMake(2, 2)];
}

-(void)createEnemies
{
    for(int i = 0; i < 5; i++){
        Enemy *enemy = [[Enemy alloc] initWithColor:[SKColor redColor] size:CGSizeMake(50, 50)];
        enemy.position = CGPointMake(40 + (80 * i), self.size.height + (enemy.size.height * 0.5));
        [self addChild:enemy];
    }
}

-(CGFloat)getDegreesRatioBetweenY:(NSInteger)highY andY:(NSInteger)lowY
{
    CGFloat rangeY = PADDLE_Y_MAX - lowY;
    CGFloat degreesPer = rangeY / 60.0;
    CGFloat totalDegrees = (highY * degreesPer) - 90;
    if(totalDegrees < 0){
        totalDegrees = 0;
    }
    return DEGREES_TO_RADIANS(totalDegrees);
}

-(void)movePaddles:(NSSet *)touches
{
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        NSInteger yRestrict = PADDLE_Y_MAX;
        
        location.y = location.y + 60;
        
        if(location.y > yRestrict){
            location.y = yRestrict;
        }
        CGFloat rotateAngle = [self getDegreesRatioBetweenY:location.y andY:player.position.y + 60];
        SKSpriteNode *currPaddle;
        
        if(location.x < self.size.width * 0.5){
            currPaddle = leftPaddle;
            rotateAngle = rotateAngle * -1;
//            if(location.x < player.position.x - 120){
//                curveControlAdjust = 0;
//            }
//            else{
//                curveControlAdjust = 120;
//            }
        }
        else{
            currPaddle = rightPaddle;
            
        }
        currPaddle.position = location;
        [currPaddle runAction:[SKAction rotateToAngle:rotateAngle duration:0]];
        
        
        
        [self drawArms];
    }
}

-(void)drawArms
{
    curveControlAdjust = 120 - (player.position.x - leftPaddle.position.x);
    if(curveControlAdjust < 0){
        curveControlAdjust = 0;
    }
    
    curveControlAdjustRight = 120 - (rightPaddle.position.x - player.position.x);
    if(curveControlAdjustRight < 0){
        curveControlAdjustRight = 0;
    }
    
    
    if(!leftArm){
        leftArm = [SKShapeNode node];
        leftArm.position = CGPointMake(0,0);
        [self addChild:leftArm];
    }
    if(!rightArm){
        rightArm = [SKShapeNode node];
        rightArm.position = CGPointMake(0,0);
        [self addChild:rightArm];
    }
    
    SKColor *armColor = [SKColor colorWithRed:253.0/255.0 green:205.0/255.0 blue:145.0/255.0 alpha:1.0];
    
    CGMutablePathRef leftArmPath = CGPathCreateMutable();
    CGPathMoveToPoint(leftArmPath, nil, player.position.x, player.position.y);
    CGPathAddQuadCurveToPoint(leftArmPath, nil, leftPaddle.position.x - curveControlAdjust, player.position.y, leftPaddle.position.x, leftPaddle.position.y - 5);
    leftArm.path = leftArmPath;
    leftArm.strokeColor = armColor;
    leftArm.lineWidth = 16;
    
    CGMutablePathRef rightArmPath = CGPathCreateMutable();
    CGPathMoveToPoint(rightArmPath, nil, player.position.x, player.position.y);
    CGPathAddQuadCurveToPoint(rightArmPath, nil, rightPaddle.position.x + curveControlAdjustRight, player.position.y, rightPaddle.position.x, rightPaddle.position.y - 5);
    
    rightArm.path = rightArmPath;
    rightArm.strokeColor = armColor;
    rightArm.lineWidth = 16;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    [self movePaddles:touches];
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self movePaddles:touches];
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    
    
    
}

-(void)didBeginContact:(SKPhysicsContact *)contact
{
    
}

@end
